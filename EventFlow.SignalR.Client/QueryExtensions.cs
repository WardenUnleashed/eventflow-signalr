﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using EventFlow.Queries;
using EventFlow.SignalR.Queries;

namespace EventFlow.SignalR.Client
{
    public static class QueryExtensions
    {
        private static readonly Regex NameRegex = new Regex(
            @"^(Old){0,1}(?<name>[\p{L}\p{Nd}]+?)(V(?<version>[0-9]+)){0,1}$",
            RegexOptions.Compiled);
        public static QueryDefinition GetDefinition(this IQuery command)
        {
            var versionedType = command.GetType();
            var definitionFromAttribute = CreateDefinitionFromAttribute(versionedType).FirstOrDefault();
            return definitionFromAttribute ?? CreateDefinitionFromName(versionedType);
        }


        public static QueryDefinition CreateDefinitionFromName(Type versionedType)
        {
            var match = NameRegex.Match(versionedType.Name);
            if (!match.Success)
            {
                throw new ArgumentException($"Versioned type name '{versionedType.Name}' is not a valid name");
            }

            var version = 1;
            var groups = match.Groups["version"];
            if (groups.Success)
            {
                version = int.Parse(groups.Value);
            }

            var name = match.Groups["name"].Value;
            return new QueryDefinition(
                version,
                versionedType,
                name);
        }

        public static IEnumerable<QueryDefinition> CreateDefinitionFromAttribute(Type versionedType)
        {
            return versionedType
                .GetTypeInfo()
                .GetCustomAttributes()
                .OfType<QueryDefinition>()
                .Select(a => new QueryDefinition(a.Version, versionedType, a.Name));
        }
    }
}
