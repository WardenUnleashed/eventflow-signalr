﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.Commands;
using EventFlow.Core;
using EventFlow.Queries;
using EventFlow.SignalR.Requests;
using Microsoft.AspNetCore.SignalR.Client;

namespace EventFlow.SignalR.Client
{
    public class EventFlowSignalRClient
    {
        
        private readonly HubConnection _connection;
        private readonly IJsonSerializer _jsonSerializer;

        public EventFlowSignalRClient(IJsonSerializer jsonSerializer, HubConnection connection)
        {
            _jsonSerializer = jsonSerializer ?? throw new ArgumentNullException(nameof(jsonSerializer));
            _connection = connection;
        }

        public async Task<IExecutionResult> PublishCommand<TAggregate, TIdentity, TExecutionResult>(ICommand<TAggregate, TIdentity, TExecutionResult> command, CancellationToken cancellationToken = default)
            where TIdentity: IIdentity
            where TAggregate : IAggregateRoot<TIdentity>
            where TExecutionResult: IExecutionResult
        {
            var definition = command.GetDefinition();
            var publishCommandRequest = new PublishCommandRequest()
            {
                Command = _jsonSerializer.Serialize(command),
                Name = definition.Name,
                Version = definition.Version
            };
            var result = await _connection.InvokeAsync<IExecutionResult>("PublishCommand", publishCommandRequest, cancellationToken).ConfigureAwait(false);
            return result;
        }

        public async Task<T> ProcessQuery<T>(IQuery<T> query, CancellationToken cancellationToken = default)
        {
            var definition = query.GetDefinition();
            var processQueryRequest = new ProcessQueryRequest()
            {
                Query = _jsonSerializer.Serialize(query),
                Name = definition.Name,
                Version = definition.Version,
            };
            var result = await _connection.InvokeAsync<T>("ProcessQuery", processQueryRequest, cancellationToken).ConfigureAwait(false);
            return result;
        }
    }
}
