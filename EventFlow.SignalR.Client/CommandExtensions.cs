﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using EventFlow.Commands;

namespace EventFlow.SignalR.Client
{
    public static class CommandExtensions
    {
        private static readonly Regex NameRegex = new Regex(
            @"^(Old){0,1}(?<name>[\p{L}\p{Nd}]+?)(V(?<version>[0-9]+)){0,1}$",
            RegexOptions.Compiled);
        public static CommandDefinition GetDefinition(this ICommand command)
        {
            var versionedType = command.GetType();
            var definitionFromAttribute = CreateDefinitionFromAttribute(versionedType).FirstOrDefault();
            return definitionFromAttribute ?? CreateDefinitionFromName(versionedType);
        }


        public static CommandDefinition CreateDefinitionFromName(Type versionedType)
        {
            var match = NameRegex.Match(versionedType.Name);
            if (!match.Success)
            {
                throw new ArgumentException($"Versioned type name '{versionedType.Name}' is not a valid name");
            }

            var version = 1;
            var groups = match.Groups["version"];
            if (groups.Success)
            {
                version = int.Parse(groups.Value);
            }

            var name = match.Groups["name"].Value;
            return new CommandDefinition(
                version,
                versionedType,
                name);
        }

        public static IEnumerable<CommandDefinition> CreateDefinitionFromAttribute(Type versionedType)
        {
            return versionedType
                .GetTypeInfo()
                .GetCustomAttributes()
                .OfType<CommandVersionAttribute>()
                .Select(a => new CommandDefinition(a.Version, versionedType, a.Name));
        }
    }
}
