﻿using EventFlow.Core;

namespace EventFlow.SignalR.Example.Domain.Timers
{
    public class PersistentStopwatchId : Identity<PersistentStopwatchId>
    {
        public PersistentStopwatchId(string value) : base(value)
        {
        }
    }
}
