﻿using System;
using EventFlow.Aggregates;
using EventFlow.SignalR.Example.Domain.Timers.Events;
using NodaTime;

namespace EventFlow.SignalR.Example.Domain.Timers
{
    public class PersistentStopwatchAggregate : AggregateRoot<PersistentStopwatchAggregate, PersistentStopwatchId>,
                                                IEmit<StopwatchCreated>,
                                                IEmit<StopwatchStarted>,
                                                IEmit<StopwatchStopped>
    {

        public PersistentStopwatchAggregate(PersistentStopwatchId id) : base(id)
        {
        }
        
        public PersistentStopwatch Stopwatch { get; private set; }

        public void Create(IMetadata metadata = null)
        {
            if(!IsNew) throw new ArgumentException("Stopwatch already exists");
            Emit(new StopwatchCreated(), metadata);
        }

        public void Start(Instant startInstant, IMetadata metadata = null)
        {
            if(IsNew) Create(metadata);
            Emit(new StopwatchStarted(startInstant), metadata);
        }
        public void Stop(Instant stopInstant, IMetadata metadata = null)
        {
            if(IsNew) throw new ArgumentException("Stopwatch must exist in order to stop it");
            Emit(new StopwatchStarted(stopInstant), metadata);
        }

        public void Apply(StopwatchCreated aggregateEvent)
        {
            Stopwatch = new PersistentStopwatch(Id);
        }
        public void Apply(StopwatchStarted aggregateEvent)
        {
            Stopwatch.Start(aggregateEvent.InstantStarted);
        }

        public void Apply(StopwatchStopped aggregateEvent)
        {
            Stopwatch.Stop(aggregateEvent.StopInstant);
        }
    }
}
