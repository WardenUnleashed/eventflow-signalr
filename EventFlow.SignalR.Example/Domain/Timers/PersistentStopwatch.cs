﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventFlow.Entities;
using NodaTime;

namespace EventFlow.SignalR.Example.Domain.Timers
{

    public class PersistentStopwatch: Entity<PersistentStopwatchId>
    {
        private Instant? _startTime;
        private readonly List<Interval> _completedIntervals = new List<Interval>();
        public PersistentStopwatch(PersistentStopwatchId id) : base(id)
        {
        }

        public Duration TimeElapsed => _completedIntervals.Aggregate(Duration.Zero, (duration, interval) => duration + interval.Duration);
        
        public void Start(Instant instant)
        {
            if(_startTime != null) throw new ArgumentException("Stopwatch has already been started");
            _startTime = instant;
        }

        public void Stop(Instant instant)
        {
            if(_startTime == null) throw new ArgumentException("Stopwatch must be started in order to stop it");
            if(_startTime < instant) throw new ArgumentException("Cannot stop timer before it was started");
            _completedIntervals.Add(new Interval(_startTime.Value, instant));
            _startTime = null;
        }
    }

}
