﻿using EventFlow.Aggregates;
using NodaTime;

namespace EventFlow.SignalR.Example.Domain.Timers.Events
{
    public class StopwatchStarted: AggregateEvent<PersistentStopwatchAggregate, PersistentStopwatchId>
    {
        public StopwatchStarted(Instant instantStarted)
        {
            InstantStarted = instantStarted;
        }
        public Instant InstantStarted { get; }
    }
}
