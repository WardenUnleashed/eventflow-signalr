﻿using EventFlow.Aggregates;

namespace EventFlow.SignalR.Example.Domain.Timers.Events
{
    public class StopwatchCreated: AggregateEvent<PersistentStopwatchAggregate, PersistentStopwatchId>
    {}
}
