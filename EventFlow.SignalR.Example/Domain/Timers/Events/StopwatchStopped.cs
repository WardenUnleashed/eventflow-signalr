﻿using EventFlow.Aggregates;
using NodaTime;

namespace EventFlow.SignalR.Example.Domain.Timers.Events
{
    public class StopwatchStopped: AggregateEvent<PersistentStopwatchAggregate, PersistentStopwatchId>
    {
        public StopwatchStopped(Instant stopInstant)
        {
            StopInstant = stopInstant;
        }

        public Instant StopInstant { get; }
    }
}
