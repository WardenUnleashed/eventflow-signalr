﻿
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.Commands;
using EventFlow.Core;

namespace EventFlow.SignalR.Example.Domain.Timers.Commands
{
    public class CreateStopWatch: Command<PersistentStopwatchAggregate, PersistentStopwatchId, IExecutionResult>
    {

        public CreateStopWatch(PersistentStopwatchId aggregateId = null, SourceId sourceId = null) 
            : base(aggregateId ?? PersistentStopwatchId.New, sourceId ?? new SourceId(CommandId.New.Value))
        {

        }
    }

    public class CreateStopWatchCommandHandler: CommandHandler<PersistentStopwatchAggregate, PersistentStopwatchId, IExecutionResult, CreateStopWatch>
    {
        public override Task<IExecutionResult> ExecuteCommandAsync(PersistentStopwatchAggregate aggregate, CreateStopWatch command, CancellationToken cancellationToken)
        {
            aggregate.Create();
            return Task.FromResult(ExecutionResult.Success());
        }
    }
}
