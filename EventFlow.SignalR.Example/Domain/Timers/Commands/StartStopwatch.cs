﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.Commands;
using EventFlow.Core;
using NodaTime;

namespace EventFlow.SignalR.Example.Domain.Timers.Commands
{
    public class StartStopwatch : Command<PersistentStopwatchAggregate, PersistentStopwatchId, IExecutionResult>
    {
        public Instant StartInstant { get; }
        public StartStopwatch() : base(PersistentStopwatchId.New) { }
        public StartStopwatch(PersistentStopwatchId aggregateId, Instant startInstant) : base(aggregateId)
        {
            StartInstant = startInstant;
        }
        public StartStopwatch(PersistentStopwatchId aggregateId, ISourceId sourceId, Instant startInstant) : base(aggregateId, sourceId)
        {
            StartInstant = startInstant;
        }
    }

    public class StartStopwatchCommandHandler : CommandHandler<PersistentStopwatchAggregate, PersistentStopwatchId, IExecutionResult, StartStopwatch>
    {
        public override Task<IExecutionResult> ExecuteCommandAsync(PersistentStopwatchAggregate aggregate, StartStopwatch command, CancellationToken cancellationToken)
        {
            aggregate.Start(command.StartInstant);
            return Task.FromResult(ExecutionResult.Success());
        }
    }
}
