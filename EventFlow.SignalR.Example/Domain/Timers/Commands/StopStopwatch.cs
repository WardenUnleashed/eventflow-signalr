﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.Commands;
using EventFlow.Core;
using NodaTime;

namespace EventFlow.SignalR.Example.Domain.Timers.Commands
{
    public class StopStopwatch : Command<PersistentStopwatchAggregate, PersistentStopwatchId, IExecutionResult>
    {
        public Instant StopInstant { get; }
        public StopStopwatch() : base(PersistentStopwatchId.New) { }
        public StopStopwatch(PersistentStopwatchId aggregateId, Instant stopInstant) : base(aggregateId)
        {
            StopInstant = stopInstant;
        }
        public StopStopwatch(PersistentStopwatchId aggregateId, ISourceId sourceId, Instant stopInstant) : base(aggregateId, sourceId)
        {
            StopInstant = stopInstant;
        }
    }

    public class StopStopwatchCommandHandler : CommandHandler<PersistentStopwatchAggregate, PersistentStopwatchId, IExecutionResult, StopStopwatch>
    {
        public override Task<IExecutionResult> ExecuteCommandAsync(PersistentStopwatchAggregate aggregate, StopStopwatch command, CancellationToken cancellationToken)
        {
            aggregate.Stop(command.StopInstant);
            return Task.FromResult(ExecutionResult.Success());
        }
    }
}