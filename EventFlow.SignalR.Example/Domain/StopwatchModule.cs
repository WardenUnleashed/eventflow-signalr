﻿using EventFlow.Configuration;
using EventFlow.Extensions;

namespace EventFlow.SignalR.Example.Domain
{
    public class StopwatchModule: IModule
    {
        public void Register(IEventFlowOptions eventFlowOptions)
        {
            eventFlowOptions.AddDefaults(typeof(StopwatchModule).Assembly);
        }
    }
}
