using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore;

namespace EventFlow.SignalR.Example
{
    public class StopwatchProgram
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder<Startup>(args);
    }
}
