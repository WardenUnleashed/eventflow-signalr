﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Aggregates;
using EventFlow.Queries;
using EventFlow.SignalR.Example.Domain.Timers;
using EventFlow.SignalR.Queries;

namespace EventFlow.SignalR.Example.Queries
{
    public class StopwatchExists: SerializableQuery<bool>
    {
        public StopwatchExists(PersistentStopwatchId id)
        {
            Id = id;
        }

        public PersistentStopwatchId Id { get;  }
    }

    public class StopWatchExistsQueryHandler : IQueryHandler<StopwatchExists, bool>
    {
        private readonly IAggregateStore _aggregateStore;
        public StopWatchExistsQueryHandler(IAggregateStore aggregateStore)
        {
            _aggregateStore = aggregateStore ?? throw new ArgumentNullException(nameof(aggregateStore));
        }
        public async Task<bool> ExecuteQueryAsync(StopwatchExists query, CancellationToken cancellationToken)
        {
            var aggregate = await _aggregateStore.LoadAsync<PersistentStopwatchAggregate, PersistentStopwatchId>(query.Id, cancellationToken).ConfigureAwait(false);
            var exists = !aggregate.IsNew;
            return exists;

        }
    }
}
