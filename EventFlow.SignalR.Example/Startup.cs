using EventFlow.DependencyInjection.Extensions;
using EventFlow.EventStores.InMemory;
using EventFlow.Extensions;
using EventFlow.Logs;
using EventFlow.SignalR.Example.Domain;
using EventFlow.SignalR.Example.Queries;
using EventFlow.SignalR.Queries;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace EventFlow.SignalR.Example
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging((builder) =>
            {
                builder.AddConsole();
                builder.SetMinimumLevel(LogLevel.Debug);
                builder.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Debug);
                builder.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug);
                builder.AddFilter("EventFlow", LogLevel.Debug);

            });
            services.AddEventFlow(options =>
                options
                    .UseConsoleLog()
                    .ConfigureJson(JsonOptionsBuilder.Build)
                    .RegisterServices(registration => registration.Register<IQueryDefinitionService>(
                        (context) =>
                        {
                            var service = new QueryDefinitionService(context.Resolver.Resolve<ILog>());
                            service.Load(typeof(StopwatchExists));
                            return service;
                        })
                    )
                    .RegisterModule<StopwatchModule>()
            );
            services.AddSignalR()
                .AddNewtonsoftJsonProtocol((options) => options.PayloadSerializerSettings = JsonOptionsBuilder.Build(options.PayloadSerializerSettings))
                .AddHubOptions<EventFlowHub>(options => options.EnableDetailedErrors = true);
            services.AddHostedService<HostedBootstrapper>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
                endpoints.MapHub<EventFlowHub>("/event-flow");
            });
        }
    }
}
