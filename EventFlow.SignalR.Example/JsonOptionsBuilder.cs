﻿
using Newtonsoft.Json;
using EventFlow.Configuration.Serialization;
using EventFlow.Extensions;
using EventFlow.ValueObjects;

namespace EventFlow.SignalR.Example
{
    public class JsonOptionsBuilder
    {
        public static JsonOptions BuildOptions()
        {
            var options = JsonOptions.New;
            options.AddConverter<ExecutionResultConverter>();
            return options;
        }
        public static JsonOptions Build(JsonOptions options)
        {
            options.AddConverter<SingleValueObjectConverter>();
            options.AddConverter<ExecutionResultConverter>();
            return options;
        }
        public static JsonSerializerSettings Build(JsonSerializerSettings settings)
        {
            settings ??= new JsonSerializerSettings();
            settings.Converters.Add(new SingleValueObjectConverter());
            settings.Converters.Add(new ExecutionResultConverter());
            return settings;
        }
    }
}
