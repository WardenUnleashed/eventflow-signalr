﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Configuration.Bootstraps;
using Microsoft.Extensions.Hosting;

namespace EventFlow.SignalR.Example
{
    public class HostedBootstrapper : IHostedService
    {
        private readonly IBootstrapper _bootstrapper;
        private bool _started;

        public HostedBootstrapper(IBootstrapper bootstrapper)
        {
            _bootstrapper = bootstrapper;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            if (_started)
            {
                return;
            }
            await _bootstrapper.StartAsync(cancellationToken).ConfigureAwait(false);
            _started = true;

        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            if (!_started)
            {
                return Task.CompletedTask;
            }

            _started = false;
            return Task.CompletedTask;
        }
    }
}
