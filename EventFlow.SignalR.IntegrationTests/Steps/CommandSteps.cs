﻿using System;
using System.Threading.Tasks;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.SignalR.Client;
using EventFlow.SignalR.Example.Domain.Timers;
using EventFlow.SignalR.Example.Domain.Timers.Commands;
using EventFlow.SignalR.Example.Queries;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace EventFlow.SignalR.IntegrationTests.Steps
{
    [Binding]
    public class CommandSteps
    {
        private readonly EventFlowSignalRClient _client;
        private IExecutionResult _executionResult;
        private PersistentStopwatchId _persistentStopwatchId = null;
        public CommandSteps(EventFlowSignalRClient client)
        {
            _client = client;
        }

        [When(@"I create a stopwatch")]
        public async Task WhenICreateAStopwatch()
        {
            var createStopwatch = new CreateStopWatch();
            _persistentStopwatchId = createStopwatch.AggregateId;
            _executionResult = await _client.PublishCommand(createStopwatch).ConfigureAwait(true);

        }

        [Then(@"I should have received a successful execution result")]
        public void ThenIShouldHaveReceivedASuccessfulExecutionResult()
        {
            _executionResult.Should().NotBeNull();
            _executionResult.IsSuccess.Should().BeTrue();
        }

        [Then(@"a stopwatch should have been created")]
        public async Task ThenAStopwatchShouldHaveBeenCreated()
        {
            var query = new StopwatchExists(_persistentStopwatchId);
            var created = await _client.ProcessQuery(query).ConfigureAwait(true);
            created.Should().BeTrue();
        }


    }
}
