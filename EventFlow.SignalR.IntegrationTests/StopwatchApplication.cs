﻿using System;
using EventFlow.SignalR.Example;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace EventFlow.SignalR.IntegrationTests
{
    public class StopwatchApplication
    {
        public static StopwatchApplication Instance = new StopwatchApplication();

        public TestServer Server { get; private set; }

        public IHubConnectionBuilder HubBuilder { get; set; }
        private StopwatchApplication() {}

        public void Initialize()
        {
            var builder = StopwatchProgram.CreateHostBuilder(Array.Empty<string>());
            Server = new TestServer(builder);
   
            HubBuilder = new HubConnectionBuilder()
                .WithUrl($"http://localhost:{Server.BaseAddress.Port}/event-flow", (options) => options.HttpMessageHandlerFactory = (_) => Server.CreateHandler())
                .WithAutomaticReconnect()
                .ConfigureLogging(x =>
                
                    x.AddConsole()
                    .SetMinimumLevel(LogLevel.Debug)
                    .AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Debug)
                    .AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug)
                    .AddFilter("EventFlow", LogLevel.Debug)

                )
                .AddNewtonsoftJsonProtocol(options => options.PayloadSerializerSettings = JsonOptionsBuilder.Build(options.PayloadSerializerSettings));
        }

    }
}
