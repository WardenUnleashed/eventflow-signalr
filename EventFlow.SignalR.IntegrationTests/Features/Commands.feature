﻿Feature: Commands

Scenario: A Client Should Be Able To Publish a Command and Receive Results When Successful
When I create a stopwatch
Then I should have received a successful execution result
And a stopwatch should have been created