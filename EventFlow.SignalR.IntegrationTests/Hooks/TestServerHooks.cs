﻿using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace EventFlow.SignalR.IntegrationTests.Hooks
{
    [Binding]
    public static class TestServerHooks
    {
        
        [BeforeTestRun]
        public static void SetupTestServer()
        {
            StopwatchApplication.Instance.Initialize();
        }
    }
}
