﻿using System;
using System.Threading.Tasks;
using EventFlow.Core;
using EventFlow.SignalR.Client;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;

namespace EventFlow.SignalR.IntegrationTests.Hooks
{
    [Binding]
    public class ScenarioSetupHooks
    {
        private readonly ScenarioContext _context;
        public ScenarioSetupHooks(ScenarioContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        [BeforeScenario()]
        public void SetupClient()
        {
            _context.ScenarioContainer.RegisterInstanceAs(StopwatchApplication.Instance.HubBuilder);
            _context.ScenarioContainer.RegisterFactoryAs((container) =>
            {
                var connectionBuilder = container.Resolve<IHubConnectionBuilder>();
                var connection = connectionBuilder.Build();
                var task = Task.Run(async () => await connection.StartAsync().ConfigureAwait(false));
                task.Wait();
                return new EventFlowSignalRClient(StopwatchApplication.Instance.Server.Services.GetService<IJsonSerializer>(), connection);
            });
        }
    }
}
