﻿using System.Threading.Tasks;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.SignalR.Requests;

namespace EventFlow.SignalR
{
    public interface IEventFlowHubClient
    {
        public Task<IExecutionResult> PublishCommand(PublishCommandRequest request);
        public Task<string> ProcessQuery(ProcessQueryRequest request);

    }
}
