﻿using System;
using EventFlow.Aggregates.ExecutionResults;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EventFlow.SignalR
{
    public class ExecutionResultConverter: JsonConverter
    {

        public static Type ExecutionResultType = typeof(IExecutionResult);

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            switch (value)
            {
                case SuccessExecutionResult success:
                    serializer.Serialize(writer, success, typeof(SuccessExecutionResult));
                    break;
                case FailedExecutionResult failed:
                    serializer.Serialize(writer, failed, typeof(FailedExecutionResult));
                    break;
            }
        }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var token = JToken.Load(reader);
            if (token.Value<bool>("isSuccess"))
            {
                return token.ToObject<SuccessExecutionResult>();
            }
            return token.ToObject<FailedExecutionResult>();
        }
        public override bool CanConvert(Type objectType)
        {
            return objectType == ExecutionResultType;

        }
    }
}