﻿using System;
using EventFlow.Core.VersionedTypes;

namespace EventFlow.SignalR.Queries
{
    [AttributeUsage(AttributeTargets.Class)]
    public class QueryVersionAttribute : VersionedTypeAttribute
    {
        public QueryVersionAttribute(
            string name,
            int version)
            : base(name, version)
        {
        }
    }
}
