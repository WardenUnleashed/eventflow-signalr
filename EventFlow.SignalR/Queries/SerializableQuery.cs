﻿using System.Threading;
using System.Threading.Tasks;
using EventFlow.Queries;

namespace EventFlow.SignalR.Queries
{
    public interface ISerializableQuery: IQuery
    {
        public Task<object> ProcessAsync(IQueryProcessor processor, CancellationToken token = default);
    }
    public abstract class SerializableQuery<T>: IQuery<T>, ISerializableQuery
    {
        public async Task<object> ProcessAsync(IQueryProcessor processor, CancellationToken token = default)
        {
            var result = await processor.ProcessAsync(this, token).ConfigureAwait(false);
            return result;
        }
    }
}
