﻿using EventFlow.Core;

namespace EventFlow.SignalR.Queries
{
    public class QueryId: Identity<QueryId>
    {
        public QueryId(string value) : base(value)
        {
        }
    }
}
