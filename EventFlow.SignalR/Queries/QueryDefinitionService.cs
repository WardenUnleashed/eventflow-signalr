﻿using System;
using EventFlow.Core.VersionedTypes;
using EventFlow.Logs;
using EventFlow.Queries;

namespace EventFlow.SignalR.Queries
{
    public interface IQueryDefinitionService : IVersionedTypeDefinitionService<QueryVersionAttribute, QueryDefinition>
    {
    }
    public class QueryDefinitionService : VersionedTypeDefinitionService<IQuery, QueryVersionAttribute, QueryDefinition>,  IQueryDefinitionService
    {
        public QueryDefinitionService(ILog log) : base(log)
        {
        }
        protected override QueryDefinition CreateDefinition(int version, Type type, string name)
        {
            return new QueryDefinition(version, type, name);
        }
    }
}
