﻿using System;
using EventFlow.Core.VersionedTypes;

namespace EventFlow.SignalR.Queries
{
    public class QueryDefinition : VersionedTypeDefinition
    {
        public QueryDefinition(
            int version,
            Type type,
            string name)
            : base(version, type, name)
        {
        }
    }
}
