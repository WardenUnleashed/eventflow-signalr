﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EventFlow.Queries;

namespace EventFlow.SignalR.Queries
{
    public static class QueryExtensions
    {

        public static async Task<object> ProcessAsync(this IQuery query, IQueryProcessor queryProcessor, CancellationToken token = default)
        {
            if(!(query is ISerializableQuery serializableQuery)) throw new ArgumentException("query must implement 'ISerializableQuery' interface");

            var result = await serializableQuery.ProcessAsync(queryProcessor, token).ConfigureAwait(false);
            return result;
        }
    }
}
