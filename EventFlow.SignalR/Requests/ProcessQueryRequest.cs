﻿namespace EventFlow.SignalR.Requests
{
    public class ProcessQueryRequest
    {
        public string Name { get; set; }
        public int Version { get; set; }
        public string Query { get; set; }

    }
}
