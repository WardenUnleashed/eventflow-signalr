﻿namespace EventFlow.SignalR.Requests
{
    public class PublishCommandRequest
    {
        public string Name { get; set; }
        public int Version { get; set; }
        public string Command { get; set; }
    }
}
