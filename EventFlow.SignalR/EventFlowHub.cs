﻿using System;
using System.Threading.Tasks;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.Commands;
using EventFlow.Core;
using EventFlow.Queries;
using EventFlow.SignalR.Queries;
using EventFlow.SignalR.Requests;
using Microsoft.AspNetCore.SignalR;

namespace EventFlow.SignalR
{
    public class EventFlowHub: Hub
    {
        private readonly ICommandDefinitionService _commandDefinitionService;
        private readonly IJsonSerializer _jsonSerializer;
        private readonly ICommandBus _commandBus;
        private readonly IQueryDefinitionService _queryDefinitionService;
        private readonly IQueryProcessor _queryProcessor;


        public EventFlowHub(ICommandDefinitionService commandDefinitionService, IJsonSerializer jsonSerializer, ICommandBus commandBus, IQueryProcessor queryProcessor, IQueryDefinitionService queryDefinitionService)
        {
            _commandDefinitionService = commandDefinitionService ?? throw new ArgumentNullException(nameof(commandDefinitionService));
            _jsonSerializer = jsonSerializer ?? throw new ArgumentNullException(nameof(jsonSerializer));
            _commandBus = commandBus ?? throw new ArgumentNullException(nameof(commandBus));
            _queryProcessor = queryProcessor ?? throw new ArgumentNullException(nameof(queryProcessor));
            _queryDefinitionService = queryDefinitionService ?? throw new ArgumentNullException(nameof(queryDefinitionService));
        }
        public async Task<IExecutionResult> PublishCommand(PublishCommandRequest request)
        {
            var commandDefinition = _commandDefinitionService.GetDefinition(request.Name, request.Version);
            var serializedCommand = (ICommand)_jsonSerializer.Deserialize(request.Command, commandDefinition.Type);
            var executionResult = await serializedCommand.PublishAsync(_commandBus, Context.ConnectionAborted).ConfigureAwait(false);
            return executionResult;
        }

        public async Task<object> ProcessQuery(ProcessQueryRequest request)
        {
            var queryDefinition = _queryDefinitionService.GetDefinition(request.Name, request.Version);
            var serializedQuery = (IQuery) _jsonSerializer.Deserialize(request.Query, queryDefinition.Type);
            var result = await serializedQuery.ProcessAsync(_queryProcessor, Context.ConnectionAborted).ConfigureAwait(false);
            return result;
        }
    }
}
